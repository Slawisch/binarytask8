export class Fighter {
    _id;
    name;
    source;
    constructor(id, name, source) {
        this._id = id;
        this.name = name;
        this.source = source;
    }
}
