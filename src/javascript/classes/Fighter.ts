export class Fighter{
    _id: string;
    name: string;
    source: string;

    constructor(id : string, name : string, source : string){
        this._id = id;
        this.name = name;
        this.source = source;
    }
}