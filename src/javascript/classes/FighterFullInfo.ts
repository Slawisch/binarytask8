export class FighterFullInfo{
    _id: string;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;

    constructor(
        id : string,
        name : string,
        health : number,
        attack : number,
        defence : number,
        source : string){
        this._id = id;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defence;
        this.source = source;
    }
}