export class FighterFullInfo {
    _id;
    name;
    health;
    attack;
    defense;
    source;
    constructor(id, name, health, attack, defence, source) {
        this._id = id;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defence;
        this.source = source;
    }
}
