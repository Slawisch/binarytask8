import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { Fighter } from './classes/Fighter';

const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');

export async function startApp() {
  try {
      if(loadingElement != null)
        loadingElement.style.visibility = 'visible';
    
    const fighters : Fighter[] = <Fighter[]>await getFighters();
    const fightersElement = createFighters(fighters);

    if(rootElement != null)
        rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    if(rootElement != null)
        rootElement.innerText = 'Failed to load data';
  } finally {
    if(loadingElement != null)
        loadingElement.style.visibility = 'hidden';
  }
}
