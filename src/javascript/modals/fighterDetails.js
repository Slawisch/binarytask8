import { createElement } from "../helpers/domHelper";
import { showModal } from './modal';
import { KeyValuePair } from "../classes/KeyValue";
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal(title, bodyElement);
}
function createFighterDetails(fighter) {
    const name = fighter.name;
    const attack = '' + fighter.attack;
    const defense = '' + fighter.defense;
    const health = '' + fighter.health;
    const image = fighter.source;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
    const attackElement = createElement({ tagName: 'div', className: 'fighter-attack' });
    const defenseElement = createElement({ tagName: 'div', className: 'fighter-defense' });
    const healthElement = createElement({ tagName: 'div', className: 'fighter-health' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: [new KeyValuePair('src', image)] });
    // show fighter name, attack, defense, health, image
    nameElement.innerText = "Name: " + name;
    attackElement.innerText = "Attack: " + attack;
    defenseElement.innerText = "Defense: " + defense;
    healthElement.innerText = "Health: " + health;
    fighterDetails.append(nameElement);
    fighterDetails.append(attackElement);
    fighterDetails.append(defenseElement);
    fighterDetails.append(healthElement);
    fighterDetails.append(imageElement);
    return fighterDetails;
}
