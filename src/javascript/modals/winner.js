import { showModal } from './modal';
import { createElement } from "../helpers/domHelper";
import { KeyValuePair } from "../classes/KeyValue";
export function showWinnerModal(fighter) {
    // show winner name and image
    const title = "Winner";
    const name = fighter.name;
    const image = fighter.source;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: [new KeyValuePair('src', image)] });
    nameElement.innerText = name;
    fighterDetails.append(nameElement);
    fighterDetails.append(imageElement);
    showModal(title, fighterDetails);
}
