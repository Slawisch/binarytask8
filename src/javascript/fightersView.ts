import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';
import { Fighter } from './classes/Fighter';
import { FighterFullInfo } from './classes/FighterFullInfo';

export function createFighters(fighters : Fighter[])
{
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event : Event, fighter : Fighter)
{
  const fullInfo : FighterFullInfo =  await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) {
  // get fighter form fightersDetailsCache or use getFighterDetails function
    const info : FighterFullInfo =  await getFighterDetails(fighterId);
    return info;
}

function createFightersSelector() {
  const selectedFighters = new Map<string, FighterFullInfo>();

  return async function selectFighterForBattle(event : Event, fighter : Fighter) 
{
    const fullInfo = await getFighterInfo(fighter._id);

    if(event.target != null)
    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const winner = fight(Array.from(selectedFighters.values())[0], Array.from(selectedFighters.values())[1]);
      showWinnerModal(winner);
    }
  }
}
