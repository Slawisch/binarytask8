export function createElement(elementForCreation) {
    const element = document.createElement(elementForCreation.tagName);
    if (elementForCreation.className !== undefined) {
        element.classList.add(elementForCreation.className);
    }
    if (elementForCreation.attributes !== undefined) {
        elementForCreation.attributes.forEach(el => element.setAttribute(el.key, el.value));
    }
    return element;
}
