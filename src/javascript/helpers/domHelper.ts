import {KeyValuePair} from '../classes/KeyValue';

export function createElement(elementForCreation : { tagName : string, className?: string, attributes?: KeyValuePair[]}){
    const element : HTMLElement = document.createElement(elementForCreation.tagName);
    
    if (elementForCreation.className !== undefined) {
      element.classList.add(elementForCreation.className);
    }
  
    if(elementForCreation.attributes !== undefined){
        elementForCreation.attributes.forEach(el => element.setAttribute(el.key, el.value));
    }
    return element;
  }