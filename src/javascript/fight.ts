import { randomInt } from "crypto";
import { Fighter } from "./classes/Fighter";
import { FighterFullInfo } from "./classes/FighterFullInfo";

export function fight(firstFighter : FighterFullInfo, secondFighter : FighterFullInfo) {
    let winner : Fighter;
    let attackSide = Math.floor(Math.random() * 2) + 1;

  console.log(firstFighter);

    while(firstFighter.health > 0 && secondFighter.health > 0){

      if(attackSide === 1){
        secondFighter.health -= getDamage(firstFighter, secondFighter);
        attackSide = 2;
      }
      else if (attackSide === 2){
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        attackSide = 1;
      }
    }

    if(attackSide === 1)
      winner = secondFighter;
    else
      winner = firstFighter;

    return winner;
  }
  
  export function getDamage(attacker : FighterFullInfo, enemy : FighterFullInfo) {
    // damage = hit - block
    // return damage 

    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(enemy);


    const damage = hitPower >= blockPower ? hitPower - blockPower : 0;
    return damage; 
  }
  
  export function getHitPower(fighter : FighterFullInfo) {
    // return hit power
    const criticalHitChance  =  Math.floor(Math.random() * 2) + 1;
    const hitPower = fighter.attack * criticalHitChance;  
    return hitPower;
  }
  
  export function getBlockPower(fighter : FighterFullInfo) {
    // return block power
    const dodgeChance =  Math.floor(Math.random() * 2) + 1;
    const blockPower = fighter.defense * dodgeChance;
    return blockPower;
  }
  